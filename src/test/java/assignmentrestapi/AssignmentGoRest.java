package assignmentrestapi;

import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

   public class AssignmentGoRest 
   {
  
	@Test(priority = 0)
	public void testStatusCode() 
	{
		baseURI = "https://gorest.co.in/";
		given().get("public/v2/users").then().statusCode(200);
	}	
	
	@Test(priority = 1)
	public void firstGetMethod() 
	{
		Response res = RestAssured.get("https://gorest.co.in/public/v2/users");
		int statusCode = res.getStatusCode();
		System.out.println(statusCode);
		System.out.println(res.getBody().asString());
	}	
	
	@Test(priority = 2)
	private void assertGetMethod()
	{		
		Response res = RestAssured.get("https://gorest.co.in/public/v2/users");
		int statusCode = res.getStatusCode();	
		Assert.assertEquals(200, statusCode);	
		
	}	
	@Test(priority = 3)
	public void postOperationTest() 
	{
		JSONObject req = new JSONObject();
		req.put("name", "Aaditya");
		req.put("email", "aadityapatil@gmail.com");
		req.put("gender", "male");
		req.put("status", "Active");
		System.out.println(req);
		baseURI = "https://gorest.co.in/public/v2";
	    ValidatableResponse res = given().log().all().contentType("application/json")
				.header("authorization", 
						"Bearer 3846b690bee1fd1f2ae522d7dd4f7f2be4eb8036137474ec0857ab00226eb003")
				.body(req.toJSONString()).when().post("/users").then().statusCode(201);
			
	}
	
	@Test(priority = 4)
	public void putOperationTest() 
	{
		JSONObject req = new JSONObject();
		req.put("name", "Aaditya");
		req.put("email", "aadityapatil@gmail.com");
		req.put("gender", "male");
		req.put("status", "Active");
		System.out.println(req);
		baseURI = "https://gorest.co.in/public/v2";
		given().log().all().contentType("application/json")
				.header("authorization", 
						"Bearer 3846b690bee1fd1f2ae522d7dd4f7f2be4eb8036137474ec0857ab00226eb003")
				.body(req.toJSONString()).when().put("/users/14504").then()
				.statusCode(200);	
	}	
	
	@Test(priority = 5)
	public void DeleteOperationTest() 
	{
		baseURI = "https://gorest.co.in/public/v2";
		given().log().all().contentType("application/json")
				.header("authorization", 
						"Bearer 3846b690bee1fd1f2ae522d7dd4f7f2be4eb8036137474ec0857ab00226eb003")
				.when().delete("/users/14504").then().statusCode(204);	
	}
 
  
}
