package restapitesting;

import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;

import org.json.simple.JSONObject;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;

public class PutDataGoRest {

	@Test
	public void firstPutMethod() {
		Response res = RestAssured.put("https://gorest.co.in/public/v2/users/181767");
		int statusCode = res.getStatusCode();
		System.out.println(statusCode);
		System.out.println(res.getBody().asString());

	} 
	@Test
	public void putData() {

		JSONObject req = new JSONObject();
		req.put("name","Aaditya QA");
		req.put("email", "aadityapatil111@gmail.com");
		req.put("gender", "male");
		req.put("status", "Active");
		System.out.println(req);
		baseURI = "https://gorest.co.in/public/v2";

		given().log().all().contentType("application/json").header("authorization","Bearer "
				+ "3846b690bee1fd1f2ae522d7dd4f7f2be4eb8036137474ec0857ab00226eb003").
		body(req.toJSONString()).when().put("/users/56843").then().statusCode(200);
	}


}

