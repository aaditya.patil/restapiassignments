package restapitesting;

import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

import org.json.simple.JSONObject;

public class PostandGetDataGoRest {
	@Test
    public void testStatusCode() {
        baseURI = "https://gorest.co.in/";
        given().get("public/v2/users").then().statusCode(200);
}
    @Test
    public void firstGetMethod() {
        Response res = RestAssured.get("https://gorest.co.in/public/v2/users/56843");
        int statusCode = res.getStatusCode();
        System.out.println(statusCode);
        System.out.println(res.getBody().asString());
        
    } 
    @Test
    private void assertGetMethod() {    
    	Response res = RestAssured.get("https://gorest.co.in/public/v2/users");
        int statusCode = res.getStatusCode();
        Assert.assertEquals(200, statusCode);   
     }
    
     @Test
     public void postOperationTest() {
    	 JSONObject req = new JSONObject();
         req.put("name","Aaditya");
         req.put("email", "aditya12@gmail.com");
         req.put("gender", "male");
         req.put("status", "Active");
         System.out.println(req);
         baseURI = "https://gorest.co.in/public/v2";
        
   given().log().all().contentType("application/json").header("authorization","Bearer "
   		+ "dad43fac2bd5aafcb039e7b1c10923290a4a6ea15dc61349afc621e8c55a2994").body(req.toJSONString()).when().post("/users").then().statusCode(201);
      }
}
