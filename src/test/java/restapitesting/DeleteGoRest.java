package restapitesting;

import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;

import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;

public class DeleteGoRest {
	
	@Test
    public void testStatusCode() {
        baseURI = "https://gorest.co.in/";
        given().get("public/v2/users").then().statusCode(200);
}
	
  @Test
  public void deleteMethod() {
      Response res = RestAssured.delete("https://gorest.co.in/public/v2/users/56843");
      int statusCode = res.getStatusCode();
      System.out.println(statusCode);
      System.out.println(res.getBody().asString());
      
  } 
  
  @Test
  private void assertGetMethod() {    
  	Response res = RestAssured.delete("https://gorest.co.in/public/v2/users/56843");
      int statusCode = res.getStatusCode();
      Assert.assertEquals(204, statusCode);   
   }
}
