package apitesting;

import org.testng.annotations.Test;
import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

public class TestingDeleteRequest {
  @Test
  public void deleteOperation()  {
	  
	  baseURI="https://reqres.in/";
	  given().when().delete("api/users/2").then().statusCode(204);
  }
}
