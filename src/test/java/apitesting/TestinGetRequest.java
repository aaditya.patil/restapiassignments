package apitesting;

import org.testng.Assert;

import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;


public class TestinGetRequest {
  @Test
  public void firstgetMethod() {
	  
	  Response response= RestAssured.get("https://reqres.in/api/users?page=2"); 
	  int statuscode=response.getStatusCode();
	  System.out.println(statuscode);
	  System.out.println(response.getBody().asString());
  }
  
  @Test
  public void assertGetMethod()
  {
	  Response response= RestAssured.get("https://reqres.in/api/users?page=2");
	  int statuscode=response.getStatusCode();
	  Assert.assertEquals(200, statuscode);
  }
}
